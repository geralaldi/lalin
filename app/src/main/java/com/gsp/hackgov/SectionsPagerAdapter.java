package com.gsp.hackgov;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gsp.hackgov.fragment.FeedFragment;
import com.gsp.hackgov.fragment.MapsFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

	public SectionsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a PlaceholderFragment (defined as a static inner class below).

		switch (position) {
			case 0:
				return FeedFragment.newInstance(1);
			case 1:
				return MapsFragment.newInstance(null, null);
			case 2:
				break;
		}
		return FeedFragment.newInstance(1);
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
			case 0:
				return "Feed";
			case 1:
				return "Maps";
			case 2:
				return "Forum";
		}
		return null;
	}
}
