package com.gsp.hackgov;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.appevents.AppEventsLogger;
import com.parse.ParseUser;

public class SplashActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
	}

	private void goToLoginActivity() {
		Intent i = new Intent(this, LoginActivity.class);
		startActivity(i);
		finish();
	}

	private void goToMainActivity() {
		Intent i = new Intent(this, MainActivity.class);
		startActivity(i);
		finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		AppEventsLogger.activateApp(this);

		ParseUser user = ParseUser.getCurrentUser();

		if (user == null) {
			goToLoginActivity();
		} else {
			goToMainActivity();
		}
	}
}
