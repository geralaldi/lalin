package com.gsp.hackgov.model;

import android.os.AsyncTask;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

/**
 * Created by Lenovo on 11/28/2015.
 */
public class Photo extends ParseObject {

	ParseFile file;
	/**
	 * can look at CONFIG.listType
	 * 1 = User photo/avatar
	 * 2 = Post photo
	 */
	int type;
	public static ParseFile defaultAvatar;

	/**
	 * this is for setting default photo
	 *
	 * @param data
	 * @param fileName
	 */
	public void setDefault(byte[] data, String fileName) {
//		ParseFile newAvatar = new ParseFile(fileName, data);
//		newAvatar.saveInBackground(new SaveCallback() {
//			public void done(ParseException e) {
//				// Handle success or failure here ...
//				defaultAvatar = newAvatar;
//			}
//		}, new ProgressCallback() {
//			public void done(Integer percentDone) {
//				// Update your progress spinner here. percentDone will be between 0 and 100.
//			}
//		});
	}

	public static ParseObject newInstance(byte[] dataInput, String fileNameInput, int type) {
		//Photo newPhoto = new Photo();
		//saving image to parse
		ParseFile newFile = new ParseFile(fileNameInput, dataInput);
		newFile.saveInBackground(new SaveCallback() {
			public void done(ParseException e) {
				// Handle success or failure here ...
			}
		}, new ProgressCallback() {
			public void done(Integer percentDone) {
				// Update your progress spinner here. percentDone will be between 0 and 100.
			}
		});


		ParseObject newPhoto = new ParseObject("Photo");
		newPhoto.put("file", newFile);
		newPhoto.put("type", type);
		newPhoto.saveEventually();

		return newPhoto;
	}
}
