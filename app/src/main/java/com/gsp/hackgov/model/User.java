package com.gsp.hackgov.model;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lenovo on 11/28/2015.
 */
@ParseClassName("User")
public class User extends ParseObject {

	public String getUserName() {
		return getString("name");
	}

	public void setUserName(String value) {
		put("name", value);
	}

	public ParseFile getAvatar() {
		return getParseFile("avatar");
	}

	public void setAvatar(ParseFile value) {
		put("avatar", value);
	}

	public String getStatus() {
		return getString("status");
	}

	public void setStatus(String value) {
		put("status", value);
	}

	public String getFacebook() {
		return getString("facebook");
	}

	public void setFacebook(String value) {
		put("facebook", value);
	}

	public String getTwitter() {
		return getString("twitter");
	}

	public void setTwitter(String value) {
		put("twitter", value);
	}

	public String getGoogle() {
		return getString("google");
	}

	public void setGoogle(String value) {
		put("google", value);
	}

	/**
	 * @return list of Post
	 */
	public List<Post> getFavorite() {
		return getList("favorite");
	}

	public void setFavorite(List<Post> value) {
		put("favorite", value);
	}

	public void addFavorite(Post value) {
		List newList = getList("favorite");
		if (newList == null) {
			newList = new LinkedList<Post>();
		}

		newList.add(value);
		put("favorite", value);
	}

	/**
	 * blom jadi
	 *
	 * @param value
	 * @return
	 */
	public List<ParseObject> findWithName(String value) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
		query.whereEqualTo("name", value);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> userList, ParseException e) {
				if (e == null) {
					//return userList;
				} else {
				}
			}
		});
		return null;
	}
}
