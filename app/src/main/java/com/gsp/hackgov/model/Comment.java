package com.gsp.hackgov.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Lenovo on 11/28/2015.
 */
@ParseClassName("Comment")
public class Comment extends ParseObject {
	public User getUser() {
		return getUser();
	}

	public void setUser(User value) {
		put("user", value);
	}

	public String getUserId() {
		return getString("userId");
	}

	public void setUserId(String value) {
		put("userId", value);
	}

	public Post getPost() {
		return getPost();
	}

	public void setPost(Post value) {
		put("post", value);
	}

	public String getPostId() {
		return getString("postId");
	}

	public void setPostId(String value) {
		put("postId", value);
	}

	public String getContent() {
		return getString("content");
	}

	public void setContent(String value) {
		put("content", value);
	}

}
