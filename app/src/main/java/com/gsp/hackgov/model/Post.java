package com.gsp.hackgov.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lenovo on 11/28/2015.
 */
@ParseClassName("Post")
public class Post extends ParseObject {
	public User getUser() {
		return getUser();
	}

	public void setUser(User value) {
		put("user", value);
	}

	public String getUserId() {
		return getString("userId");
	}

	public void setUserId(String value) {
		put("userId", value);
	}

	public int getCategory() {
		return getInt("category");
	}

	public void setCategory(int value) {
		put("category", value);
	}

	public String getTitle() {
		return getString("title");
	}

	public void setTitle(String value) {
		put("title", value);
	}

	public String getContent() {
		return getString("content");
	}

	public void setContent(String value) {
		put("content", value);
	}

	public ParseGeoPoint getLocation() {
		return getParseGeoPoint("location");
	}

	public void setGeoPoint(ParseGeoPoint value) {
		put("location", value);
	}

	public int getNumberOfUpvotes() {
		return getInt("numberOfUpVotes");
	}

	public void set(int value) {
		put("numberOfUpVotes", value);
	}

	public ParseFile getPhoto() {
		return getParseFile("photo");
	}

	public void setPhoto(ParseFile value) {
		put("photo", value);
	}

	public int getSolved() {
		return getInt("solved");
	}

	public void setSolved(int value) {
		put("solved", value);
	}

	public List<Comment> getComments() {
		return getList("comments");
	}

	public void setComments(List<Comment> value) {
		put("comments", value);
	}

	public void addComment(Comment value) {
		List<Comment> newComments = getList("comments");

		if (newComments == null) {
			newComments = new LinkedList<Comment>();
			newComments.add(value);
			put("comments", value);
		} else {
			newComments.add(value);
			put("comments", newComments);
		}
	}

	public List<User> getFavorites() {
		return getList("favorites");
	}

	public void setFavorites(List<User> value) {
		put("favorites", value);
	}

	public void addFavorite(User value) {
		List<User> newFavorites = getList("favorites");

		if (newFavorites == null) {
			newFavorites = new LinkedList<User>();
		}
		newFavorites.add(value);
		put("favorites", newFavorites);
	}

	public int getFavoritesNumber() {
		return getFavorites().size();
	}

	public List<User> getUpVotes() {
		return getList("upVotes");
	}

	public void setUpVotes(List<User> value) {
		put("upVotes", value);
	}

	public void addUpVote(User value) {
		List<User> newUpVotes = getList("upVotes");

		if (newUpVotes == null) {
			newUpVotes = new LinkedList<User>();
		}
		newUpVotes.add(value);
		put("upVotes", newUpVotes);
	}

	public int getUpVotesNumber() {
		return getUpVotes().size();
	}
}
