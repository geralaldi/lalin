package com.gsp.hackgov.twitter;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.models.User;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by geraldi on 11/29/15.
 */
public interface UsersService {
	@GET("/1.1/users/show.json")
	void show(@Query("user_id") Long userId,
	          Callback<User> cb);
}
