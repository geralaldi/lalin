package com.gsp.hackgov.twitter;

import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.TwitterApiClient;

/**
 * Created by geraldi on 11/29/15.
 */
public class UsersShowTwitterApiClient extends TwitterApiClient {
	public UsersShowTwitterApiClient(Session session) {
		super(session);
	}

	public UsersService getUsersService() {
		return getService(UsersService.class);
	}
}
