package com.gsp.hackgov.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.gsp.hackgov.model.Comment;
import com.gsp.hackgov.model.Post;
import com.gsp.hackgov.model.User;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseTwitterUtils;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;

/**
 * Created by geraldi on 11/28/15.
 */
public class LalinApplication extends Application {

	// Note: Your consumer key and secret should be obfuscated in your source code before shipping.
	private static final String TWITTER_KEY = "w6bgqOnHT0SufNv95kOYDKkJl";
	private static final String TWITTER_SECRET = "RFl4cC7mhrfPMqmtcyqJZgzuiPgUyoxtqE7JEtNwagomDhFCKX";

	private static LalinApplication instance;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;

		TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
		Fabric.with(this, new TwitterCore(authConfig));

		// Enable Local Datastore.
		Parse.enableLocalDatastore(this);

		Parse.initialize(this, "tIAuT5qGDiOU5HFPEEuqhpgUFNerIgFGOCQEZR5f", "Ezp5zGXD7wedpL4gkcqNDJGqTFMTuLAxwV2uGjYm");
		ParseFacebookUtils.initialize(this);
		ParseTwitterUtils.initialize(TWITTER_KEY, TWITTER_SECRET);

		ParseObject.registerSubclass(User.class);
		ParseObject.registerSubclass(Comment.class);
		ParseObject.registerSubclass(Post.class);
	}

	public static synchronized LalinApplication getSharedInstance() {
		return instance;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}
}
