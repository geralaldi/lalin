package com.gsp.hackgov;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AddPostActivity extends AppCompatActivity {
	private static final String TAG = AddPostActivity.class.getSimpleName();

	public static final int GALLERY_REQUEST_CODE = 101;

	private ImageView userImageView;
	private TextView userNameTextView;
	private EditText postTitleEditText;
	private EditText postDetailEditText;
	private LinearLayout locationButtonView;
	private Button locationButton;
	private TextView locationNameTextView;

	private LinearLayout photoButtonView;
	private Button photoButton;
	private TextView photoNameTextView;

	private ImageView selectedImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_post);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);


		userImageView = (ImageView) findViewById(R.id.userImageView);
		userNameTextView = (TextView) findViewById(R.id.userNameTextView);
		postTitleEditText = (EditText) findViewById(R.id.postTitleEditText);
		postDetailEditText = (EditText) findViewById(R.id.postDetailEditText);

		locationButtonView = (LinearLayout) findViewById(R.id.locationButtonView);
		locationButtonView.setClickable(true);
		locationButtonView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Log.d(TAG, "location button onclick");
			}
		});

		locationButton = (Button) findViewById(R.id.locationButton);
		locationNameTextView = (TextView) findViewById(R.id.locationNameTextView);

		photoButtonView = (LinearLayout) findViewById(R.id.photoButtonView);
		photoButtonView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.setType("image/*");
				startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
			}
		});

		photoButton = (Button) findViewById(R.id.addPhotoButton);
		photoNameTextView = (TextView) findViewById(R.id.photoTextView);

		selectedImageView = (ImageView) findViewById(R.id.selectedImageView);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

			Uri selectedImageUri = data.getData();
			String[] projection = {MediaStore.MediaColumns.DATA};
			CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
					null);
			Cursor cursor = cursorLoader.loadInBackground();
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
			cursor.moveToFirst();
			String selectedImagePath = cursor.getString(column_index);
			Bitmap bm;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(selectedImagePath, options);
			final int REQUIRED_SIZE = 400;
			int scale = 1;
			while (options.outWidth / scale / 2 >= REQUIRED_SIZE
					&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;
			options.inSampleSize = scale;
			options.inJustDecodeBounds = false;
			bm = BitmapFactory.decodeFile(selectedImagePath, options);
			selectedImageView.setImageBitmap(bm);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_add_post, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_post) {

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private Uri getUri() {
		String state = Environment.getExternalStorageState();
		if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
			return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

		return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	}
}
