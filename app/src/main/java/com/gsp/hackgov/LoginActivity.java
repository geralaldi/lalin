package com.gsp.hackgov;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {
	private static final String TAG = LoginActivity.class.getSimpleName();

	private Button fbLoginButton;
	private Button twitterLoginButton;

	private CallbackManager callbackManager;
	private AccessTokenTracker accessTokenTracker;
	private AccessToken accessToken;

	private ParseUser parseUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		callbackManager = CallbackManager.Factory.create();

		fbLoginButton = (Button) findViewById(R.id.fbLoginButton);
		fbLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginActivity.this, Arrays.asList(getResources().getStringArray(R.array.fb_read_permission)), new LogInCallback() {
					@Override
					public void done(ParseUser user, ParseException e) {
						parseUser = user;
						accessToken = AccessToken.getCurrentAccessToken();
						goToMainActivity();
					}
				});
			}
		});

		twitterLoginButton = (Button) findViewById(R.id.twitterLoginButton);
		twitterLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ParseTwitterUtils.logIn(LoginActivity.this, new LogInCallback() {
					@Override
					public void done(ParseUser user, ParseException e) {
						parseUser = user;
						goToMainActivity();
					}
				});
			}
		});

		accessTokenTracker = new AccessTokenTracker() {
			@Override
			protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
				accessToken = currentAccessToken;
			}
		};

		accessToken = AccessToken.getCurrentAccessToken();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
	}

	private void goToMainActivity() {
		Intent i = new Intent(this, MainActivity.class);
		startActivity(i);
		finish();
	}

}
