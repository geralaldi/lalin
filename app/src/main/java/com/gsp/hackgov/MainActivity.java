package com.gsp.hackgov;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.gsp.hackgov.fragment.FeedFragment;
import com.gsp.hackgov.fragment.dummy.DummyContent;
import com.gsp.hackgov.twitter.UsersShowTwitterApiClient;
import com.makeramen.roundedimageview.RoundedImageView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.twitter.Twitter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener, FeedFragment.OnListFragmentInteractionListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link FragmentPagerAdapter} derivative, which will keep every
	 * loaded fragment in memory. If this becomes too memory intensive, it
	 * may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	private SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;
	private NavigationView navigationView;

	private ParseUser user;

	private ParseFile photo;

	Bitmap bitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.container);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(mViewPager);

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		navigationView.setItemIconTintList(null);

		user = ParseUser.getCurrentUser();

		setupNavHeader();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_search) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupNavHeader() {
		View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
		final RoundedImageView userImageView = (RoundedImageView) headerView.findViewById(R.id.imageView);
		final TextView usernameTextView = (TextView) headerView.findViewById(R.id.textView);

		if (ParseFacebookUtils.isLinked(user)) {
			GraphRequest meRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
				@Override
				public void onCompleted(JSONObject object, GraphResponse response) {
					try {
						usernameTextView.setText(object.getString("name"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/me/picture",
					new GraphRequest.Callback() {
						@Override
						public void onCompleted(GraphResponse response) {
							Transformation transformation = new RoundedTransformationBuilder()
									.cornerRadiusDp(userImageView.getWidth())
									.oval(true)
									.build();

							try {
								Picasso.with(MainActivity.this).load(response.getJSONObject().getJSONObject("data").getString("url")).fit().transform(transformation).into
										(userImageView);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});

			Bundle params = new Bundle();
			params.putString("type", "large");
			params.putBoolean("redirect", false);
			request.setParameters(params);
			request.executeAsync();

			meRequest.executeAsync();
		} else if (ParseTwitterUtils.isLinked(user)) {
			Twitter twitter = ParseTwitterUtils.getTwitter();

			TwitterAuthToken twitterAuthToken = new TwitterAuthToken(twitter.getAuthToken(), twitter.getAuthTokenSecret());

			TwitterSession session = new TwitterSession(twitterAuthToken, Integer.parseInt(twitter.getUserId()),
					ParseTwitterUtils.getTwitter().getScreenName());

			UsersShowTwitterApiClient client = new UsersShowTwitterApiClient(session);
			client.getUsersService().show(Long.parseLong(twitter.getUserId()), new Callback<User>() {
				@Override
				public void success(Result<User> result) {
					Transformation transformation = new RoundedTransformationBuilder()
							.cornerRadiusDp(userImageView.getWidth())
							.oval(true)
							.build();
					Picasso.with(MainActivity.this).load(result.data.profileImageUrl).transform(transformation).into(userImageView);
					usernameTextView.setText(result.data.name);
				}

				@Override
				public void failure(TwitterException e) {

				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	@Override
	public void onListFragmentInteraction(DummyContent.DummyItem item, ImageView postImageView) {
		Intent i = new Intent(this, PostDetailActivity.class);
		ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, postImageView, "photo");
		startActivity(i, options.toBundle());
	}

	@Override
	public void onAddPostButtonClicked() {
		Intent i = new Intent(this, AddPostActivity.class);
		startActivity(i);
	}
}
