package com.gsp.hackgov.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsp.hackgov.R;
import com.gsp.hackgov.fragment.FeedFragment.OnListFragmentInteractionListener;
import com.gsp.hackgov.fragment.dummy.DummyContent.DummyItem;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyFeedRecyclerViewAdapter extends RecyclerView.Adapter<MyFeedRecyclerViewAdapter.ViewHolder> {

	private final List<DummyItem> mValues;
	private final OnListFragmentInteractionListener mListener;

	public MyFeedRecyclerViewAdapter(List<DummyItem> items, OnListFragmentInteractionListener listener) {
		mValues = items;
		mListener = listener;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.fragment_feed, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		holder.mItem = mValues.get(position);

		holder.mView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (null != mListener) {
					// Notify the active callbacks interface (the activity, if the
					// fragment is attached to one) that an item has been selected.
					mListener.onListFragmentInteraction(holder.mItem, holder.postImageView);
				}
			}
		});
	}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public final View mView;
		public final RoundedImageView userImageView;
		public final TextView userNameTextView;
		public final TextView postTimeTextView;
		public final Button favouriteButton;
		public final TextView postTitleTextView;
		public final TextView postDetailTextView;
		public final ImageView postImageView;
		public final Button locationButton;
		public final TextView distanceTextView;
		public final TextView postInfoTextView;
		public final Button voteButton;
		public final Button commentButton;
		public DummyItem mItem;

		public ViewHolder(View view) {
			super(view);
			mView = view;
			postImageView = (ImageView) view.findViewById(R.id.postImageView);
			userImageView = (RoundedImageView) view.findViewById(R.id.userImageView);
			userNameTextView = (TextView) view.findViewById(R.id.userNameTextView);
			postTimeTextView = (TextView) view.findViewById(R.id.postTimeTextView);
			favouriteButton = (Button) view.findViewById(R.id.favouriteButton);
			postTitleTextView = (TextView) view.findViewById(R.id.postTitleTextView);
			postDetailTextView = (TextView) view.findViewById(R.id.postDetailTextView);
			locationButton = (Button) view.findViewById(R.id.locationButton);
			distanceTextView = (TextView) view.findViewById(R.id.distanceTextView);
			postInfoTextView = (TextView) view.findViewById(R.id.postInfoTextView);
			voteButton = (Button) view.findViewById(R.id.voteButton);
			commentButton = (Button) view.findViewById(R.id.commentButton);
		}
	}
}
