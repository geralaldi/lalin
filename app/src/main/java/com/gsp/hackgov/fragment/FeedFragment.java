package com.gsp.hackgov.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gsp.hackgov.R;
import com.gsp.hackgov.fragment.dummy.DummyContent;
import com.gsp.hackgov.fragment.dummy.DummyContent.DummyItem;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FeedFragment extends Fragment {

	// TODO: Customize parameters
	private int mColumnCount = 1;

	// TODO: Customize parameter argument names
	private static final String ARG_COLUMN_COUNT = "column-count";

	private OnListFragmentInteractionListener mListener;

	private FloatingActionButton addPostButton;
	private RecyclerView recyclerView;

	// TODO: Customize parameter initialization
	@SuppressWarnings("unused")
	public static FeedFragment newInstance(int columnCount) {
		FeedFragment fragment = new FeedFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_COLUMN_COUNT, columnCount);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public FeedFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {
			mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_feed_list, container, false);

		recyclerView = (RecyclerView) view.findViewById(R.id.list);

		addPostButton = (FloatingActionButton) view.findViewById(R.id.addButton);
		addPostButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mListener.onAddPostButtonClicked();
			}
		});

		// Set the adapter
		Context context = recyclerView.getContext();
		if (mColumnCount <= 1) {
			recyclerView.setLayoutManager(new LinearLayoutManager(context));
		} else {
			recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
		}
		recyclerView.setAdapter(new MyFeedRecyclerViewAdapter(DummyContent.ITEMS, mListener));
		return view;
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnListFragmentInteractionListener) {
			mListener = (OnListFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnListFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnListFragmentInteractionListener {
		// TODO: Update argument type and name
		void onListFragmentInteraction(DummyItem item, ImageView postImageView);

		void onAddPostButtonClicked();
	}
}
